import time
import numpy as np
import random

# 2 Instances

FILE_SIZE = 100

# Question 2.1

# allows to read the instance file and build the corresponding cost matrix
def parser(file_name):
    x = 0
    y = 0
    matrice = [[0]*FILE_SIZE for i in range(FILE_SIZE)]
    file = open(file_name, "r")
    file = file.readlines()[7:]
    lines = FILE_SIZE
    cpt = 0
    for line in file:
        if cpt == lines:
            cpt = 0
            lines -= 1
            x += 1
            y = x
        matrice[x][y] = int(line.rstrip("\n"))
        matrice[y][x] = int(line.rstrip("\n"))
        y = y + 1
        cpt += 1
    return matrice


# Question 2.2

def one_objective_evaluation(permutation, matrix):
    weight = 0
    for i in range(len(permutation)):
        if(i+1 <len(permutation)):
            weight += matrix[permutation[i]][permutation[i+1]]    
        else:
            weight += matrix[permutation[i]][permutation[0]]
    return weight


# Question 2.3
# Generate a random path for a 100 points
def random_permutation():
    path = [i for i in range(FILE_SIZE)]
    random.shuffle(path)
    return path

def random_evaluation(matrix):
    path = random_permutation()
    return one_objective_evaluation(path, matrix)


# 3 Heuristiques constructives

# Question 3.1

# Allows to know if a point has already been visited or not
def is_already_visited(visitedPoints, point):
    for i in range(len(visitedPoints)) :
        if visitedPoints[i] == point: 
            return True
    return False


# constructive nearest neighbour heuristic for the TSP
# returns the visit order of the points
def nearest_neighbor_search(startingPoint, matrix): 
    visitedPoints = []
    row = startingPoint
    minIndex = 0

    # On doit boucler après le point de départ donné en entrée, puis on boucle sur les points rangés avant le point de départ
    for i in range(FILE_SIZE) : 
        minWeight = matrix[row][(row+1)%FILE_SIZE]
        minIndex = (row+1)%FILE_SIZE
        for col in range(FILE_SIZE) :
            weight = matrix[row][col]
            if row != col  and not is_already_visited(visitedPoints, col) and weight <= minWeight:
                minWeight = weight
                minIndex = col

        visitedPoints.append(minIndex)
        row = minIndex

    return visitedPoints
  
# TEST 
def main():
    file = "./data/randomD100.tsp"
    print("Computing on : ", file)
    matrix = parser(file)
    
    use_nearest_neighbor = input("Use nearest neighbor (yes/no): ")
    
    start = time.time_ns()
    if(use_nearest_neighbor == "yes"):
        print("Processing with nearest neibor search ...")
        path = nearest_neighbor_search(0, matrix)
    else:
        print("Processing with random permutation ...")
        path = random_permutation()
        
    weight = one_objective_evaluation(path, matrix)
    end = time.time_ns()
    duration = (end-start)/1000000
    
    print("Nearest neighbor cost : ", weight)
    print("Average calculation duration : %s ms" % duration)

if __name__ == '__main__':
    main()