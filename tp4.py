import time
import matplotlib.pyplot as plt
import numpy as np
import tp1

# first_objective -> file name of the first objective
# second_objective -> file name of the second objective
# permutation -> the path
def two_objectives_evaluation(first_objective, second_objective, permutation) :
    matrix1 = tp1.parser(first_objective)
    matrix2 = tp1.parser(second_objective)

    return (tp1.one_objective_evaluation(permutation, matrix1), tp1.one_objective_evaluation(permutation, matrix2))


def random_objectives_evaluations(size, first_objective, second_objective):
    points = []

    for i in range(size) :
        result = two_objectives_evaluation(first_objective, second_objective, tp1.random_permutation())
        points.append((result[0], result[1]))

    return points
        
def off_line(points):
    pareto_front = []

    for i in range(len(points)) :
        if(is_dominant(points[i], points)) :
            pareto_front.append(points[i])

    return pareto_front


# current_tuple 
# tuples -> ensembles des solutions connues pour les 2 objectifs
#  on check si un point est dominé par un autre point de la solution
def is_dominant(current_tuple, tuples) :
    i = 0
    while(i < len(tuples)):
        if(tuples[i][0] < current_tuple[0] and tuples[i][1] < current_tuple[1]):
            return False
        i +=  1
        
    return True

def on_line(size, points, first_objective, second_objective):
    pareto_front = []
    
    for i in range(size) :
        random_point = two_objectives_evaluation(first_objective, second_objective, tp1.random_permutation())
        pareto_front.append(random_point)
        points.append(random_point)
        pareto_front = off_line(pareto_front)

    return pareto_front

def display_plot(points, color):
    x = []
    y = []

    for i in range(len(points)):
        x.append(points[i][0])
        y.append(points[i][1])
        
    plt.plot(x, y, color)

def display_pareto_and_points(points, pareto_front):
    display_plot(points, 'bo')
    display_plot(pareto_front, 'or')
    plt.show()
    
# TEST
def main() :
    use_off_line = input("Use off line (1) or on line (2)): ")
    size = input("How many points do you want to use (number): ")
    points = []

    start = time.time()
    if(use_off_line == "1"):
        title = "Off line filter"
        print(title)
        plt.title(title)
        points = random_objectives_evaluations(int(size), "./data/randomA100.tsp", "./data/randomB100.tsp")
        pareto_front = off_line(points)
    else :
        title = "On line filter"
        print(title)
        plt.title(title)
        pareto_front = on_line(int(size), points, "./data/randomA100.tsp", "./data/randomB100.tsp")
    end = time.time()
    
    print("Calculation duration : %s s" % round(end-start, 2))
    display_pareto_and_points(points, pareto_front)

if __name__ == '__main__':
    main()