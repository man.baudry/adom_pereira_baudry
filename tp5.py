import matplotlib.pyplot as plt
import numpy as np
import tp1
import tp2
import tp4

# Question 1.1 : Développer un algorithme de type scalaire pour le mTSP à 2 objectifs.

def aggregate(lambda1, point1, lambda2, point2):
    return lambda1 * point1 + lambda2 * point2


def one_objective_evaluation(permutation, first_objective, second_objective, lambda1, lambda2):
    weight = 0
    for i in range(len(permutation)):
        if(i+1 <len(permutation)):
            weight += aggregate(lambda1, first_objective[permutation[i]][permutation[i+1]], lambda2, second_objective[permutation[i]][permutation[i+1]])
        else:
            weight += aggregate(lambda1, first_objective[permutation[i]][permutation[0]], lambda2, second_objective[permutation[i]][permutation[0]])
    return weight


def best_improvement(filter, path, matrix): 
    initial_path = path.copy()
    initial_weight = tp1.one_objective_evaluation(path, matrix)
    weight = initial_weight
    for i in range(len(path)-1):
        filter(i, i+1, path)
        new_weight = tp1.one_objective_evaluation(path, matrix)
        if(new_weight < weight):
            weight = new_weight
    if weight == initial_weight:
        path = initial_path
    return weight

# 
def scalar_mTSP(first_objective, second_objective):
    path = tp1.random_permutation()
    matrix1 = tp1.parser(first_objective)
    matrix2 = tp1.parser(second_objective)
    lambda1 = 1
    lambda2 = 0
    x = []
    y = []

    while lambda2 <= 1:
        weight1 = best_improvement(tp2.swap, path, matrix1)
        weight2 = best_improvement(tp2.swap, path, matrix2)
        x.append(weight1)
        y.append(weight2)
        
        
        total_weight = one_objective_evaluation(path, matrix1, matrix2, lambda1, lambda2)

        print(f'{lambda1} - {lambda2} : ({total_weight})')
        lambda1 = round(lambda1 - 0.05, 2)
        lambda2 = round(lambda2 + 0.05, 2)
        
    random_points = tp4.random_objectives_evaluations(500, first_objective, second_objective)
    tp4.display_plot(random_points, "bo")
    plt.plot(x, y, "or")
    plt.show()


def pareto_mTSP(first_objective, second_objective):
    return
    
# TEST
def main():
    scalar_mTSP("./data/randomA100.tsp", "./data/randomB100.tsp" )

if __name__ == '__main__':
    main()