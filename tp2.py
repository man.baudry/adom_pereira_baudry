import time
import numpy as np
import tp1 

FILE_SIZE = 100
NBR_ITERATION = 1

def swap(x, y, path):
    value = path[x] 
    path[x] = path[y]
    path[y] = value

def two_opt(x, y, path):
    while x < y :
       swap(x, y, path)
       x += 1
       y -= 1


# filter -> swap or two-opt function
# path -> random path or path defined by the nearest_neighbor_search function
# matrix -> first_improvement or best_improvement function
def first_improvement(filter, path, matrix): 
    initial_path = path.copy()
    initial_weight = tp1.one_objective_evaluation(path, matrix)
    weight = initial_weight
    i = 0
    first_improvement_found = False
    while i < len(path)-1 and not first_improvement_found:
        filter(i, i+1, path)
        new_weight = tp1.one_objective_evaluation(path, matrix)
        i += 1
        if(new_weight < weight):
            weight = new_weight
            first_improvement_found = True
    
    if initial_weight == weight :
        path = initial_path
    return weight


# filter -> swap or two-opt function
# path -> random path or path defined by the nearest_neighbor_search function
# matrix -> first_improvement or best_improvement function
def best_improvement(filter, path, matrix): 
    initial_path = path.copy()
    initial_weight = tp1.one_objective_evaluation(path, matrix)
    weight = initial_weight
    for i in range(len(path)-1):
        filter(i, i+1, path)
        new_weight = tp1.one_objective_evaluation(path, matrix)
        if(new_weight < weight):
            weight = new_weight
    
    if initial_weight == weight :
        path = initial_path
    return weight


# filter -> swap or two-opt function
# path -> random path or path defined by the nearest_neighbor_search function
# selection ->
# matrix -> first_improvement or best_improvement function
def hill_climbing(filter, path, selection, matrix):
    return selection(filter, path, matrix) 
    
# TEST 
def main():
    file = "./data/randomD100.tsp"
    print("Computing on : ", file)
    matrix = tp1.parser(file)
    
    use_nearest_neighbor = input("Use nearest neighbor (1) or random (2): ")
    use_first_improvement = input("Use first improvement (1) or best improvement (2): ")
    use_two_opt = input("Use two opt (1) or swap (2): ")
    
    cost = 0
    duration = 0
    
    start = time.time()
    for i in range(NBR_ITERATION):
        if(use_nearest_neighbor == "1"):
            path = tp1.nearest_neighbor_search(0, matrix)
        else:
            path = tp1.random_permutation()
        if(use_first_improvement == "1"):
            if(use_two_opt == "1"):
                cost += hill_climbing(two_opt, path, first_improvement, matrix)
            else:
                cost += hill_climbing(swap, path, first_improvement, matrix)
        else:
            if(use_two_opt == "1"):
                cost += hill_climbing(two_opt, path, best_improvement, matrix)
            else:
                cost += hill_climbing(swap, path, best_improvement, matrix)
    end = time.time()
    duration = ((end-start)/NBR_ITERATION)*1000
    print("Hill climbing average cost : ", cost/NBR_ITERATION)
    print("Average calculation duration : %s ms" % round(duration, 5))

if __name__ == '__main__':
    main()